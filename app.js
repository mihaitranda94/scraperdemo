const express = require('express');
const http = require('http');
const app = express();
const port = process.env.PORT || 3210;

app.get('/scrape', (req, res) => {
  const { url } = req.query;

  if (!url) {
    return res.status(400).json({ error: 'Missing "url" query parameter' });
  }

  // Make an HTTP GET request to the provided URL
  http.get(url, (response) => {
    let htmlData = '';

    // Set the encoding to UTF-8 to handle text data
    response.setEncoding('utf8');

    // Handle data chunks as they come in
    response.on('data', (chunk) => {
      htmlData += chunk;
    });

    // Handle the end of the response
    response.on('end', () => {
      // You can now process the HTML content here
      res.status(200).send(htmlData);
    });
  }).on('error', (error) => {
    res.status(500).json({ error: error.message });
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
